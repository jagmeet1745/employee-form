package com.jagmeet.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jagmeet.model.UserMV;
import com.jagmeet.model.UserVM;
import com.jagmeet.service.UserService;

/**
 * @author jagmeet
 *
 */
@RestController
@RequestMapping("/api")
public class UserController {

	@Autowired
	private UserService userService;

	private static final Logger log = LoggerFactory.getLogger(UserController.class);

	/***
	 * Provides the result
	 */
	@GetMapping("/getData")
	public List<UserMV> getData() {
		log.info("Entered in Controller");
		return userService.getData();
	}

	/***
	 * Save User Information
	 */
	@PostMapping("/saveData")
	public void saveData(@RequestBody UserVM userVm) {
		log.info("Entered in Controller");
		userService.saveData(userVm);
	}

}
