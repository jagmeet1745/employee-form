package com.jagmeet.service;

import java.util.List;

import com.jagmeet.model.UserMV;
import com.jagmeet.model.UserVM;


/**
 * @author jagmeet
 *
 */
public interface UserService {

	public List<UserMV> getData();

	public void saveData(UserVM userVm);

}
