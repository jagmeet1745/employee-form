package com.jagmeet.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jagmeet.model.UserMV;
import com.jagmeet.model.UserVM;
import com.jagmeet.repository.impl.EmployeeDAOImpl;
import com.jagmeet.service.UserService;
import java.util.List;


/**
 * @author jagmeet
 *
 */
@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private EmployeeDAOImpl employeeDAOImpl;
	private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);


	
	public List<UserMV> getData() {
		log.info("Entered in Service getData");
		return employeeDAOImpl.getAll();
	}

	public void saveData(UserVM userVm) {
		log.info("Entered in Service saveData");
		employeeDAOImpl.saveEmployee(userVm);
	}

}
