package com.jagmeet.repository;

import java.util.List;

import com.jagmeet.model.UserMV;
import com.jagmeet.model.UserVM;

/**
 * @author jagmeet
 *
 */
public interface EmployeeDAO {
	
	public void saveEmployee(UserVM userVm);

	public List<UserMV> getAll();
}
