package com.jagmeet.repository.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jagmeet.model.UserMV;
import com.jagmeet.model.UserVM;
import com.jagmeet.repository.EmployeeDAO;
import com.jagmeet.service.impl.UserServiceImpl;

/**
 * @author jagmeet
 *
 */
@Repository
public class EmployeeDAOImpl implements EmployeeDAO {
	private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);
	private static final String dbUrl = "jdbc:mysql://localhost:3306/formdata";

	/**
	 * function to save Employee data in DB
	 */
	public void saveEmployee(UserVM userVm) {

		log.info("in employeedao");
		PreparedStatement ps = null;
		String query = "insert into employee (empid, firstname, lastname, email, mobile) values (?,?,?,?,?)";

		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection(dbUrl, "root", "password");
			ps = con.prepareStatement(query);
			ps.setInt(1, userVm.getEmpId());
			ps.setString(2, userVm.getfirstName());
			ps.setString(3, userVm.getLastName());
			ps.setString(4, userVm.getEmail());
			ps.setString(5, userVm.getContactNo());
			int out = ps.executeUpdate();
			if (out != 0) {
				System.out.println("Employee saved ");
			} else
				System.out.println("Employee save failed");
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	/**
	 * function to retrieve employee data from db using arraylist
	 */
	public List<UserMV> getAll() {
		String query = "Select * from employee";
		List<UserMV> empList = new ArrayList<UserMV>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection(dbUrl, "root", "password");
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				UserMV emp = new UserMV();
				emp.setEmpId(rs.getInt("empid"));
				emp.setFirstName(rs.getString("firstname"));
				emp.setLastName(rs.getString("lastname"));
				emp.setEmail(rs.getString("email"));
				emp.setContactNo(rs.getString("mobile"));
				empList.add(emp);

			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return empList;

	}

}
